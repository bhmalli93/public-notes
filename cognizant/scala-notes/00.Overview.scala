// CHAPTER 0:  OVERVIEW
// PROBLEM: 
// PROCESS:
// EXP? Procedural Programming? Object Oriented Programming? 
// USE? 


// INTRODUCTIONS 
// me, you



// -- CHECKLIST
/* set up sharaed demo folder;  
hand out course materials; 
check scala is working on all machines */


// PRE-REQs
/*
    * previous experience with java and the JVM is expected or, 
    * at least equivalent programming knowledge
    * this course does not teach programming!
*/

// SCHEDULE (3D) + PROCESS
// take own notes!

/*
    DAY             TITLE                           1h TEACHING     30m EXERCISE
    1 AM            Introduction
    1 AM            Fundamentals
    1 PM            Flow
    1 PM            Functions

    2 AM            Object Orientation
    2 AM            Traits & Inheritance
    2 PM            Collections & Generics
    2 PM            Functional Programming

    3 AM            Special Types & Libraries 
    3 AM            Pattern Matching
    3 PM            Imports & Implicits
    3 PM            SBT & TDD
*/




// EXERCISES
/*
                                            
OBJECTIVES: Using the slides and notes, complete the exercise. 
1 PROBLEM:   Use the compiler, interpret and repl to run scala code. 

*/

// GOALS
// accelerate learning to progarm with scala 









// ASIDE:
/* 4D
DAY             TITLE                           
1 AM            Introduction
1 AM            Fundamentals
1 PM            Flow
1 PM            Functions

2 AM            OO
2 AM            Traits & Inheritance
2 PM            Collections & Generics
2 PM            FP & Combinators

3 AM            Special Types & Libraries 
3 AM            Pattern Matching
3 PM            Imports & Implicits
3 PM            SBT & TDD

4 AM            Concurrency! 
4 AM            Applications! (Spark, Play)
4 PM            Design!

*/

// TODO: Ex. 5, 12, 13
// TODO: Nt. 9!      Exp. 7, 11, 13
// TODO: Intellij SBT + TDD INTEGRATION

//RD. STAGES
// RD Nt 1-4, Ex 1-4
// RD Nt 5-9  Ex 5-9
// RD Nt 10-13 Ex 10-13

