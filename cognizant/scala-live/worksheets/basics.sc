
// bools

val age = 27
val name = "Fido"
val favCol = "Blue"

val isAdult = age >= 18
val isFido = name == "Fido"
val isFavRed = favCol == "Red"


println(isAdult)
println(isFido)
println(isFavRed)

println(isAdult && isFido && isFavRed)

println(true || true)
println(true || false)
println(false || true)
println(false || false)



