
println("C:\newfolder\tree")

println(raw"C:\newfolder\tree")
println("""C:\newfolder\tree""")

val name = "Michael"
val age = 27


val message = s"${name} is ${age * 2}"

println(message)


val height = 1.81

println(f"Height: $height%.3f")