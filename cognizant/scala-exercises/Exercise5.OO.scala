// CHAPTER:   OBJECT ORIENTATION 
// PROBLEM:   Manage item discounts at a store.
// TIME:       25 m

// Q. define a Item class with properties: name and rrp

// Q. define a ReducedItem with properties: name, rrp, discount
//.. give ReducedItem a secondary constructor with a default discount of
// 0.1 * rrp



// Q. define a DiscountRates object
//.. give it christmas, easter and summer vals with 0.1, 0.2, 0.3 respectively


// Q. create a List of ReducedItems (a basket) and caluclate the total
//.. it should contain reduced items made with the default constructor,
//..the secondary, and pass'd a val from DiscountRates



// case classes 


// REVIEW: What did you learn from this exercise?